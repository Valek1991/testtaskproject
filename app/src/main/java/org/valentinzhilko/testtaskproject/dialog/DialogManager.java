package org.valentinzhilko.testtaskproject.dialog;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;

import org.valentinzhilko.testtaskproject.MainActivity;
import org.valentinzhilko.testtaskproject.R;
import org.valentinzhilko.testtaskproject.network.AsyncHttpClient;

/**
 * Created by Valentin on 12.09.2016.
 */
public class DialogManager {

    private ProgressDialog mCurrentProgressDialog;
    private DialogFragment resultDialog;

    public DialogManager() {

    }

    public void showProgressDialog(final AsyncHttpClient client, Context context, String status){

        mCurrentProgressDialog = new ProgressDialog(context);
        mCurrentProgressDialog.setCanceledOnTouchOutside(false);
        mCurrentProgressDialog.setCancelable(false);
        mCurrentProgressDialog.setMessage(status);

        mCurrentProgressDialog.setButton(Dialog.BUTTON_NEGATIVE, context.getString(R.string.progress_dialog_button_name)
                , new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                if (client != null) {

                    client.cancelLoading();
                }
            }
        });

        mCurrentProgressDialog.show();
    }

    public void showResultDialog(String result, Context context){

        resultDialog = new ResultDialog();
        ((ResultDialog) resultDialog).setResult(result);
        resultDialog.show(((MainActivity)context).getFragmentManager(), "result");
    }

    public void hideProgressDialog(){

        if(mCurrentProgressDialog != null){

            mCurrentProgressDialog.dismiss();
        }

        mCurrentProgressDialog = null;
    }

}
