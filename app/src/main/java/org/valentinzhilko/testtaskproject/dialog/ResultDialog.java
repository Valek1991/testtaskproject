package org.valentinzhilko.testtaskproject.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;

import org.valentinzhilko.testtaskproject.R;

/**
 * Created by Valentin on 13.09.2016.
 */
public class ResultDialog extends DialogFragment {

    private String mResult;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        if(savedInstanceState != null){

            mResult = savedInstanceState.getString("result");
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.result_dialog_title));
        builder.setMessage(mResult);
        builder.setPositiveButton(getString(R.string.result_dialog_button_name), new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {

            }
        });

        return builder.create();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

        super.onSaveInstanceState(outState);
        outState.putString("result", mResult);
    }

    public void setResult(String result){

        mResult = result;
    }

}
