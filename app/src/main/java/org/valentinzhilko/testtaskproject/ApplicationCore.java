package org.valentinzhilko.testtaskproject;

import org.valentinzhilko.testtaskproject.dialog.DialogManager;
import org.valentinzhilko.testtaskproject.model.ClientResult;
import org.valentinzhilko.testtaskproject.model.Form;
import org.valentinzhilko.testtaskproject.network.AsyncHttpClient;
import org.valentinzhilko.testtaskproject.util.OnNetworkConnectionListener;

/**
 * Created by Valentin on 11.09.2016.
 */

//This class plays role of "main controller"
public class ApplicationCore {

    private MainActivity mMainActivity;
    private AsyncHttpClient mHttpClient;
    private DialogManager mDialogManager;
    private OnNetworkConnectionListener mOnNetworkConnectionListener;

    public ApplicationCore(MainActivity mainActivity){

        this.mMainActivity = mainActivity;
        mHttpClient = new AsyncHttpClient(ApplicationCore.this);
        mDialogManager = new DialogManager();
    }

    public void transferFormResponse(Form form){

        if(mOnNetworkConnectionListener != null){

            mOnNetworkConnectionListener.onFormResponse(form);
        }
    }

    public void transferClientResult(ClientResult result){

        if(mOnNetworkConnectionListener != null){

            mOnNetworkConnectionListener.onClientResult(result);
        }
    }

    public void addOnHttpResponseListener(OnNetworkConnectionListener onNetworkConnectionListener){

        mOnNetworkConnectionListener = onNetworkConnectionListener;
    }

    public void removeOnHttpResponseListener(){

        mOnNetworkConnectionListener = null;
    }

    public void release(){

        mMainActivity = null;
    }

    public MainActivity getMainActivity() {

        return mMainActivity;
    }

    public AsyncHttpClient getHttpClient(){

        return mHttpClient;
    }

    public DialogManager getDialogManager(){

        return mDialogManager;
    }

}
