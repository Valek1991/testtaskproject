package org.valentinzhilko.testtaskproject.adapter.holder;

import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import org.valentinzhilko.testtaskproject.R;

/**
 * Created by Valentin on 12.09.2016.
 */
public class NumericViewHolder extends ViewHolder {

    private EditText mFieldEtv;

    public NumericViewHolder(View itemView) {

        super(itemView);
        mFieldEtv = (EditText) itemView.findViewById(R.id.numericField);
    }

    public double getNumber(){

        if (!TextUtils.isEmpty(mFieldEtv.getText().toString())){

            return Double.parseDouble(mFieldEtv.getText().toString());
        }

        return 0.0;
    }

}
