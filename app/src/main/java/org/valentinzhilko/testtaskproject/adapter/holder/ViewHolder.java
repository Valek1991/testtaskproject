package org.valentinzhilko.testtaskproject.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import org.valentinzhilko.testtaskproject.R;

/**
 * Created by Valentin on 11.09.2016.
 */
public class ViewHolder extends RecyclerView.ViewHolder {

    private TextView mFieldTitleTv;

    public ViewHolder(View itemView) {

        super(itemView);
        View view = itemView.findViewById(R.id.recyclerViewItem);
        mFieldTitleTv = (TextView) view.findViewById(R.id.fieldTitle);
    }

    public void setTitle(String title){

        mFieldTitleTv.setText(title);
    }

}
