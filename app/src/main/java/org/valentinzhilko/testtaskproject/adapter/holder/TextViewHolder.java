package org.valentinzhilko.testtaskproject.adapter.holder;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import org.valentinzhilko.testtaskproject.R;

/**
 * Created by Valentin on 11.09.2016.
 */
public class TextViewHolder extends ViewHolder {

    private EditText mFieldEtv;

    public TextViewHolder(View itemView) {

        super(itemView);
        mFieldEtv = (EditText) itemView.findViewById(R.id.textField);
    }

    public String getText(){

        if (!TextUtils.isEmpty(mFieldEtv.getText().toString())){

            return mFieldEtv.getText().toString();
        }

        return "";
    }

}
