package org.valentinzhilko.testtaskproject.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.valentinzhilko.testtaskproject.R;
import org.valentinzhilko.testtaskproject.adapter.holder.ListViewHolder;
import org.valentinzhilko.testtaskproject.adapter.holder.NumericViewHolder;
import org.valentinzhilko.testtaskproject.adapter.holder.TextViewHolder;
import org.valentinzhilko.testtaskproject.adapter.holder.ViewHolder;
import org.valentinzhilko.testtaskproject.model.FieldForm;

import java.util.ArrayList;

/**
 * Created by Valentin on 11.09.2016.
 */
public class ComplexRecyclerAdapter extends RecyclerView.Adapter<ViewHolder> {

    private enum ITEM_TYPE {

        TEXT_TYPE,
        NUMERIC_TYPE,
        LIST_TYPE

    }

    private Context mContext;
    private ArrayList<FieldForm> mFields;

    public ComplexRecyclerAdapter(Context context ,ArrayList<FieldForm> fields) {

        mContext = context;
        mFields = fields;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == ITEM_TYPE.TEXT_TYPE.ordinal()){

            View textView = LayoutInflater.from(mContext).inflate(R.layout.recycler_view_text_item, parent, false);
            return new TextViewHolder(textView);
        } else if(viewType == ITEM_TYPE.NUMERIC_TYPE.ordinal()){

            View numericView = LayoutInflater.from(mContext).inflate(R.layout.recycler_view_numeric_item, parent, false);
            return new NumericViewHolder(numericView);
        } else {

            View listView = LayoutInflater.from(mContext).inflate(R.layout.recycler_view_list_item, parent, false);
            return new ListViewHolder(mContext, listView);
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        int itemType = getItemViewType(position);

        holder.setTitle(mFields.get(position).getTitle());

        if(itemType == ITEM_TYPE.LIST_TYPE.ordinal()){

            ((ListViewHolder)holder).showList(mFields.get(position).getValues());
        }
    }

    @Override
    public int getItemViewType(int position) {

        if(mFields.get(position).getType().equals("TEXT")){

            return ITEM_TYPE.TEXT_TYPE.ordinal();
        } else if(mFields.get(position).getType().equals("NUMERIC")){

            return ITEM_TYPE.NUMERIC_TYPE.ordinal();
        } else {

            return ITEM_TYPE.LIST_TYPE.ordinal();
        }
    }

    @Override
    public int getItemCount() {

        return mFields.size();
    }

    public FieldForm getItemByPosition(int position){

        return mFields.get(position);
    }

}
