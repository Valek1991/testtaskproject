package org.valentinzhilko.testtaskproject.adapter.holder;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import org.valentinzhilko.testtaskproject.R;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Valentin on 12.09.2016.
 */
public class ListViewHolder extends ViewHolder {

    private Context mContext;
    private Spinner mSpinner;

    public ListViewHolder(Context context, View itemView) {

        super(itemView);
        mContext = context;
        mSpinner = (Spinner) itemView.findViewById(R.id.spinner);
    }

    public void showList(HashMap<String, String> values){

        initSpinner(values);
    }

    private void initSpinner(HashMap<String, String> values){

        ArrayList<String> stringValues = new ArrayList<String>(values.values());
        ArrayAdapter<String> adapter = new ArrayAdapter(mContext, android.R.layout.simple_spinner_item,
                stringValues);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinner.setAdapter(adapter);
    }

    public String getValue(){

        if(!TextUtils.isEmpty(mSpinner.getSelectedItem().toString())){

            return mSpinner.getSelectedItem().toString();
        }

        return "";
    }

}
