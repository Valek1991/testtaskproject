package org.valentinzhilko.testtaskproject.network;

import org.valentinzhilko.testtaskproject.model.Form;
import org.valentinzhilko.testtaskproject.model.ClientRequest;
import org.valentinzhilko.testtaskproject.model.ClientResult;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit.http.POST;

/**
 * Created by Valentin on 19.09.2016.
 */
public interface IHttpClientApi {

    @retrofit2.http.POST("tt/meta")
    Call<Form> getForm();

    @retrofit2.http.POST("tt/data/")
    Call<ClientResult> sendForm(@Body ClientRequest request);

    @POST("/tt/meta")
    Form getFormRoboSpice();

}
