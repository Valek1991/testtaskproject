package org.valentinzhilko.testtaskproject.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.PendingRequestListener;
import com.octo.android.robospice.request.listener.RequestListener;

import org.valentinzhilko.testtaskproject.ApplicationCore;
import org.valentinzhilko.testtaskproject.MainActivity;
import org.valentinzhilko.testtaskproject.model.ClientRequest;
import org.valentinzhilko.testtaskproject.model.Form;
import org.valentinzhilko.testtaskproject.model.ClientResult;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Valentin on 11.09.2016.
 */
public class AsyncHttpClient {

    public static final String LOADING_BASE_URL = "http://test.clevertec.ru";

    public enum ConnectingStatus {

        LOADING,
        CANCELLING

    }

    private ApplicationCore mApplicationCore;

    @Inject
    Retrofit retrofit;

    private SpiceManager mSpiceManager;
    private FormHttpRequest mFormRequest;
    private Call<Form> mLoadingFormTask;
    private Call<ClientResult> mSendingFormTask;
    private int mConnectionStatus = -1;

    public AsyncHttpClient(ApplicationCore applicationCore) {

        mApplicationCore = applicationCore;
        mSpiceManager = new SpiceManager(FormHttpService.class);
        mFormRequest = new FormHttpRequest();
        MainActivity.getAppComponent().injectRestApi(AsyncHttpClient.this);
    }

    public void loadForm(){

        if (checkOnConnection()){

            IHttpClientApi httpClientApi = retrofit.create(IHttpClientApi.class);
            mLoadingFormTask = httpClientApi.getForm();
            mLoadingFormTask.enqueue(new Callback<Form>() {

                @Override
                public void onResponse(Call<Form> call, Response<Form> response) {

                    mConnectionStatus = -1;
                    mApplicationCore.transferFormResponse(response.body());
                }

                @Override
                public void onFailure(Call<Form> call, Throwable t) {

                    mConnectionStatus = -1;
                    mApplicationCore.transferFormResponse(null);
                    Toast.makeText(mApplicationCore.getMainActivity(), "No connection", Toast.LENGTH_LONG).show();
                }
            });
        } else {

            Toast.makeText(mApplicationCore.getMainActivity(), "No connection", Toast.LENGTH_LONG).show();
        }
    }

    //example with RoboSpice
    public void loadFormRoboSpice(){

        if (checkOnConnection()){

            mConnectionStatus = ConnectingStatus.LOADING.ordinal();
            mSpiceManager.execute(mFormRequest, "", DurationInMillis.ALWAYS_EXPIRED, new RequestListener<Form>() {

                @Override
                public void onRequestFailure(SpiceException spiceException) {

                    mConnectionStatus = -1;
                    mApplicationCore.transferFormResponse(null);
                }

                @Override
                public void onRequestSuccess(Form form) {

                    mConnectionStatus = -1;
                    mApplicationCore.transferFormResponse(form);
                }
            });
        }
    }

    public void sendForm(ClientRequest request){

        if (checkOnConnection()){

            IHttpClientApi httpClientApi = retrofit.create(IHttpClientApi.class);
            mSendingFormTask = httpClientApi.sendForm(request);

            mSendingFormTask.enqueue(new Callback<ClientResult>() {

                @Override
                public void onResponse(Call<ClientResult> call, Response<ClientResult> response) {

                    mConnectionStatus = -1;
                    mApplicationCore.transferClientResult(response.body());
                }

                @Override
                public void onFailure(Call<ClientResult> call, Throwable t) {

                    mConnectionStatus = -1;
                    mApplicationCore.transferClientResult(null);
                }
            });
        } else {

            Toast.makeText(mApplicationCore.getMainActivity(), "No connection", Toast.LENGTH_LONG).show();
        }
    }

    public void startServiceRoboSpice(){

        if (mSpiceManager != null){

            if (!mSpiceManager.isStarted()){

                mSpiceManager.start(mApplicationCore.getMainActivity());
            }
        }
    }

    public void stopServiceRoboSpice(){

        if (mSpiceManager != null){

            if(mSpiceManager.isStarted()){

                mSpiceManager.shouldStop();
            }
        }
    }

    public void continueLoadingRoboSpice(){

        if (mSpiceManager != null){

            mSpiceManager.addListenerIfPending(Form.class, "", new PendingRequestListener<Form>() {

                @Override
                public void onRequestNotFound() {

                    mConnectionStatus = -1;
                    mApplicationCore.transferFormResponse(null);
                }

                @Override
                public void onRequestFailure(SpiceException spiceException) {

                    mConnectionStatus = -1;
                    mApplicationCore.transferFormResponse(null);
                }

                @Override
                public void onRequestSuccess(Form form) {

                    mConnectionStatus = -1;
                    mApplicationCore.transferFormResponse(form);
                }
            });
        }
    }

    private boolean checkOnConnection(){

        ConnectivityManager manager = (ConnectivityManager) mApplicationCore.getMainActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = manager.getActiveNetworkInfo();

        if ((info != null) && (info.isConnected())){

            return true;
        }

        return false;
    }

    public void cancelLoading(){

        if (mSpiceManager != null){

            mSpiceManager.cancelAllRequests();
        }

        if (mLoadingFormTask != null){

            mLoadingFormTask.cancel();
        }

        if (mSendingFormTask != null){

            mSendingFormTask.cancel();
        }
    }

    public int getConnectionStatus(){

        if (mSpiceManager != null){

            return mConnectionStatus;
        }

        if (mLoadingFormTask != null){

            if (mLoadingFormTask.isExecuted()){

                mConnectionStatus = ConnectingStatus.LOADING.ordinal();
                return mConnectionStatus;
            }
        }

        if (mSendingFormTask != null){

            if (mSendingFormTask.isExecuted()){

                mConnectionStatus = ConnectingStatus.LOADING.ordinal();
                return mConnectionStatus;
            }
        }

        return -1;
    }

}
