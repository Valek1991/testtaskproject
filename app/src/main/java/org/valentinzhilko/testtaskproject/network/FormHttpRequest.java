package org.valentinzhilko.testtaskproject.network;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import org.valentinzhilko.testtaskproject.model.Form;

/**
 * Created by Valentin on 21.09.2016.
 */
public class FormHttpRequest extends RetrofitSpiceRequest<Form, IHttpClientApi> {

    public FormHttpRequest() {

        super(Form.class, IHttpClientApi.class);
    }

    @Override
    public Form loadDataFromNetwork() throws Exception {

        return getService().getFormRoboSpice();
    }
}
