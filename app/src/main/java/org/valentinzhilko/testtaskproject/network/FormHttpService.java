package org.valentinzhilko.testtaskproject.network;

import android.util.Log;

import com.octo.android.robospice.retrofit.RetrofitGsonSpiceService;

/**
 * Created by Valentin on 21.09.2016.
 */
public class FormHttpService extends RetrofitGsonSpiceService {

    @Override
    public void onCreate() {

        super.onCreate();
        addRetrofitInterface(IHttpClientApi.class);
    }

    @Override
    protected String getServerUrl() {

        return AsyncHttpClient.LOADING_BASE_URL;
    }
}
