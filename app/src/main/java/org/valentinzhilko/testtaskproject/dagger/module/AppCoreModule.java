package org.valentinzhilko.testtaskproject.dagger.module;

import android.support.annotation.NonNull;

import org.valentinzhilko.testtaskproject.ApplicationCore;
import org.valentinzhilko.testtaskproject.MainActivity;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Valentin on 23.09.2016.
 */

@Module
public class AppCoreModule {

    @Provides
    @Singleton
    public ApplicationCore provideAppCoreModule(@NonNull MainActivity mainActivity){

        return new ApplicationCore(mainActivity);
    }

}
