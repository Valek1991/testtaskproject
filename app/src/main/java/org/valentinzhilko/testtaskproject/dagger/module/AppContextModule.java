package org.valentinzhilko.testtaskproject.dagger.module;

import android.support.annotation.NonNull;

import org.valentinzhilko.testtaskproject.MainActivity;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Valentin on 22.09.2016.
 */
@Module
public class AppContextModule {

    private MainActivity mMainActivity;

    public AppContextModule(@NonNull MainActivity mainActivity) {

        mMainActivity = mainActivity;
    }

    @Provides
    @Singleton
    public MainActivity provideMainActivity(){

        return mMainActivity;
    }

}
