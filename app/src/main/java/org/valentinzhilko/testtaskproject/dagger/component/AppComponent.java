package org.valentinzhilko.testtaskproject.dagger.component;

import org.valentinzhilko.testtaskproject.MainActivity;
import org.valentinzhilko.testtaskproject.dagger.module.AppContextModule;
import org.valentinzhilko.testtaskproject.dagger.module.AppCoreModule;
import org.valentinzhilko.testtaskproject.dagger.module.RestApiModule;
import org.valentinzhilko.testtaskproject.dagger.module.UiModule;
import org.valentinzhilko.testtaskproject.network.AsyncHttpClient;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Valentin on 22.09.2016.
 */

@Component(modules = {AppContextModule.class, AppCoreModule.class, RestApiModule.class, UiModule.class})
@Singleton
public interface AppComponent {

    void injectMainActivity(MainActivity mainActivity);
    void injectRestApi(AsyncHttpClient asyncHttpClient);
}
