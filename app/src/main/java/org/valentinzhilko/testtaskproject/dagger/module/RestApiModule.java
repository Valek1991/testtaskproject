package org.valentinzhilko.testtaskproject.dagger.module;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.valentinzhilko.testtaskproject.ApplicationCore;
import org.valentinzhilko.testtaskproject.network.AsyncHttpClient;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Valentin on 22.09.2016.
 */

@Module
public class RestApiModule {
    
    private String mUrl;

    public RestApiModule(String url) {

        mUrl = url;
    }

    @Provides
    @Singleton
    public Gson provideGson(){

        GsonBuilder gsonBuilder = new GsonBuilder();
        return gsonBuilder.create();
    }

    @Provides
    @Singleton
    public Retrofit provideRetrofit(Gson gson){

        return new Retrofit.Builder()
                .baseUrl(mUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

}
