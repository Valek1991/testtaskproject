package org.valentinzhilko.testtaskproject.dagger.module;

import android.app.FragmentManager;
import android.app.FragmentTransaction;

import org.valentinzhilko.testtaskproject.MainActivity;
import org.valentinzhilko.testtaskproject.R;
import org.valentinzhilko.testtaskproject.fragment.MainFragment;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Valentin on 23.09.2016.
 */

@Module
public class UiModule {

    //Create a new fragment and assign it tag.
    //If the fragment exists we obtain it and assign to R.id.fragmentContainer.
    //This old fragment needs when we change screen orientation.
    @Provides
    @Singleton
    public MainFragment provideMainFragment(MainActivity mainActivity){

        MainFragment mainFragment = null;

        FragmentManager manager = mainActivity.getFragmentManager();
        mainFragment = (MainFragment) manager.findFragmentByTag(MainFragment.MAIN_FRAGMENT_TAG);

        if(mainFragment == null){

            mainFragment = new MainFragment();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.add(R.id.fragmentContainer ,mainFragment, MainFragment.MAIN_FRAGMENT_TAG);
            transaction.commit();
        }

        return mainFragment;
    }

}
