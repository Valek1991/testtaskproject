package org.valentinzhilko.testtaskproject.util;

import org.valentinzhilko.testtaskproject.model.ClientResult;
import org.valentinzhilko.testtaskproject.model.Form;

/**
 * Created by Valentin on 12.09.2016.
 */

//This interface corresponds for responses from a server
public interface OnNetworkConnectionListener {

    void onFormResponse(Form form);
    void onClientResult(ClientResult result);
}
