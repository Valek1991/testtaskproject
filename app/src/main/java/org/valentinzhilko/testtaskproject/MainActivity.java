package org.valentinzhilko.testtaskproject;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import org.valentinzhilko.testtaskproject.dagger.component.AppComponent;
import org.valentinzhilko.testtaskproject.dagger.component.DaggerAppComponent;
import org.valentinzhilko.testtaskproject.dagger.module.AppContextModule;
import org.valentinzhilko.testtaskproject.dagger.module.AppCoreModule;
import org.valentinzhilko.testtaskproject.dagger.module.RestApiModule;
import org.valentinzhilko.testtaskproject.dagger.module.UiModule;
import org.valentinzhilko.testtaskproject.fragment.MainFragment;
import org.valentinzhilko.testtaskproject.network.AsyncHttpClient;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity {

    private static AppComponent sAppComponent;

    @Inject
    ApplicationCore mApplicationCore;
    @Inject
    MainFragment mMainFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();

        mApplicationCore.release();
        mApplicationCore = null;
    }

    private void init(){

        sAppComponent = buildComponent();
        sAppComponent.injectMainActivity(MainActivity.this);
    }

    private AppComponent buildComponent(){

        return DaggerAppComponent.builder()
                .appContextModule(new AppContextModule(MainActivity.this))
                .appCoreModule(new AppCoreModule())
                .restApiModule(new RestApiModule(AsyncHttpClient.LOADING_BASE_URL))
                .uiModule(new UiModule())
                .build();
    }

    public static AppComponent getAppComponent(){

        return sAppComponent;
    }

    public ApplicationCore getApplicationCore(){

        return mApplicationCore;
    }

}
