package org.valentinzhilko.testtaskproject.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Valentin on 12.09.2016.
 */
public class ClientResponse {

    private String mName;
    private String mValue;

    public ClientResponse(String name, String value) {

        this.mName = name;
        this.mValue = value;
    }

    public String getName() {

        return mName;
    }

    public void setName(String name) {

        this.mName = name;
    }

    public String getValue() {

        return mValue;
    }

    public void setValue(String value) {

        this.mValue = value;
    }

}
