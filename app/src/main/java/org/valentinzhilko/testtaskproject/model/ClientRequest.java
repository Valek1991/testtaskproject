package org.valentinzhilko.testtaskproject.model;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;

/**
 * Created by Valentin on 19.09.2016.
 */

public class ClientRequest {

    @SerializedName("form")
    private HashMap<String, String> mValues;

    public ClientRequest(HashMap<String, String> values) {

        mValues = values;
    }

    public HashMap<String, String> getValues() {

        return mValues;
    }

    public void setValues(HashMap<String, String> values) {

        mValues = values;
    }

    public boolean isEmpty(){

        return (mValues == null) || (mValues.size() < 1);
    }

}
