package org.valentinzhilko.testtaskproject.model;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Valentin on 19.09.2016.
 */
public class ClientResult {

    @SerializedName("result")
    private String mResult;

    public ClientResult(String result) {

        mResult = result;
    }

    public String getResult() {

        return mResult;
    }

    public void setResult(String result) {

        mResult = result;
    }

    public boolean isEmpty(){

        return TextUtils.isEmpty(mResult);
    }

}
