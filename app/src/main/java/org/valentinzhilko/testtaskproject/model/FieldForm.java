package org.valentinzhilko.testtaskproject.model;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;

/**
 * Created by Valentin on 11.09.2016.
 */
public class FieldForm {

    @SerializedName("title")
    private String mTitle;
    @SerializedName("name")
    private String mName;
    @SerializedName("type")
    private String mType;
    @SerializedName("values")
    private HashMap<String, String> mValues;

    public FieldForm() {

    }

    public FieldForm(String title, String name, String type, HashMap<String, String> values) {

        this.mTitle = title;
        this.mName = name;
        this.mType = type;
        this.mValues = values;
    }

    public String getTitle() {

        return mTitle;
    }

    public void setTitle(String title) {

        this.mTitle = title;
    }

    public String getName() {

        return mName;
    }

    public void setName(String name) {

        this.mName = name;
    }

    public String getType() {

        return mType;
    }

    public void setType(String type) {

        this.mType = type;
    }

    public HashMap<String, String> getValues() {

        return mValues;
    }

    public void setValues(HashMap<String, String> values) {

        this.mValues = values;
    }

}
