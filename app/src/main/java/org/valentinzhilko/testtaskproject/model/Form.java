package org.valentinzhilko.testtaskproject.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Valentin on 11.09.2016.
 */
public class Form {

    @SerializedName("title")
    private String mTitle;
    @SerializedName("fields")
    private ArrayList<FieldForm> mFields;

    public Form() {

    }

    public Form(String title, ArrayList<FieldForm> fields) {
        this.mTitle = title;
        this.mFields = fields;
    }

    public String getTitle() {

        return mTitle;
    }

    public void setTitle(String title) {

        this.mTitle = title;
    }

    public ArrayList<FieldForm> getFields() {

        return mFields;
    }

    public void setFields(ArrayList<FieldForm> fields) {

        this.mFields = fields;
    }

}
