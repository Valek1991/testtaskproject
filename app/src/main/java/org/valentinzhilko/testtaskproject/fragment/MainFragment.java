package org.valentinzhilko.testtaskproject.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.valentinzhilko.testtaskproject.MainActivity;
import org.valentinzhilko.testtaskproject.R;
import org.valentinzhilko.testtaskproject.adapter.ComplexRecyclerAdapter;
import org.valentinzhilko.testtaskproject.adapter.holder.ListViewHolder;
import org.valentinzhilko.testtaskproject.adapter.holder.NumericViewHolder;
import org.valentinzhilko.testtaskproject.adapter.holder.TextViewHolder;
import org.valentinzhilko.testtaskproject.adapter.holder.ViewHolder;
import org.valentinzhilko.testtaskproject.model.ClientResult;
import org.valentinzhilko.testtaskproject.model.Form;
import org.valentinzhilko.testtaskproject.model.ClientRequest;
import org.valentinzhilko.testtaskproject.network.AsyncHttpClient;
import org.valentinzhilko.testtaskproject.util.OnNetworkConnectionListener;

import java.util.HashMap;

import javax.inject.Inject;

/**
 * Created by Valentin on 13.09.2016.
 */
public class MainFragment extends Fragment implements OnNetworkConnectionListener {

    public static final String MAIN_FRAGMENT_TAG = "main";

    //References which we need retain when we change screen orientation

    private AsyncHttpClient mHttpClient;
    private Form mForm;

    private ComplexRecyclerAdapter mAdapter;
    private LinearLayoutManager mLayoutManager;

    private TextView mFormTitleTv;
    private RecyclerView mRecyclerView;
    private Button mSendBtn;
    private Button mUpdateBtn;
    private Button mUpdateRoboSpiceBtn;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_main, null);
        init(view);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);

        //Add OnHttpResponseListener that we can obtain server responses
        ((MainActivity)getActivity()).getApplicationCore().addOnHttpResponseListener(MainFragment.this);

        if (mForm != null){

            fillRecyclerView(mForm);
        }

        //When we change screen orientation we check network status(loading, cancelling).
        //and continue loading or cancelling task.
        if(mHttpClient != null){

            if(mHttpClient.getConnectionStatus() == AsyncHttpClient.ConnectingStatus.LOADING.ordinal()){

                ((MainActivity)getActivity()).getApplicationCore().getDialogManager()
                        .showProgressDialog(mHttpClient, getActivity(), getActivity().getString(R.string.loading_progress_status_text));
            }
        }

        //Retain current AsyncHttpClient
        mHttpClient = ((MainActivity)getActivity()).getApplicationCore().getHttpClient();

        //restart RoboSpiceService and task if exists
        if (mHttpClient != null){

            mHttpClient.startServiceRoboSpice();
            mHttpClient.continueLoadingRoboSpice();
        }
    }

    @Override
    public void onStop() {

        super.onStop();
        if (mHttpClient != null){

            mHttpClient.stopServiceRoboSpice();
        }
    }

    @Override
    public void onDestroy() {

        super.onDestroy();
        ((MainActivity)getActivity()).getApplicationCore().removeOnHttpResponseListener();
    }

    private void init(View view){

        initViews(view);

        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mLayoutManager);

        initListeners();
    }

    private void initViews(View view){

        mFormTitleTv = (TextView) view.findViewById(R.id.formTitleTv);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        mSendBtn = (Button) view.findViewById(R.id.sendBtn);
        mUpdateBtn = (Button) view.findViewById(R.id.updateBtn);
        mUpdateRoboSpiceBtn = (Button) view.findViewById(R.id.updateRoboSpiceBtn);
    }

    private void initListeners(){

        mSendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ClientRequest request = getClientRequest();
                if((request != null) && (!request.isEmpty())){

                    startFormUploading(request);
                }
            }
        });

        mUpdateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startFormLoading();
            }
        });

        mUpdateRoboSpiceBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                startFormLoadingRoboSpice();
            }
        });
    }

    private void startFormLoading(){

        ((MainActivity)getActivity()).getApplicationCore().getDialogManager()
                .showProgressDialog(mHttpClient, getActivity(), getActivity().getString(R.string.loading_progress_status_text));

        mHttpClient.loadForm();
    }

    private void startFormLoadingRoboSpice(){

        ((MainActivity)getActivity()).getApplicationCore().getDialogManager()
                .showProgressDialog(mHttpClient, getActivity(), getActivity().getString(R.string.loading_progress_status_text));

        mHttpClient.startServiceRoboSpice();
        mHttpClient.loadFormRoboSpice();
    }

    private void startFormUploading(ClientRequest request){

        ((MainActivity)getActivity()).getApplicationCore().getDialogManager()
                .showProgressDialog(mHttpClient, getActivity(), getActivity().getString(R.string.loading_progress_status_text));

        mHttpClient.sendForm(request);
    }

    private ClientRequest getClientRequest(){

        HashMap<String, String> values = new HashMap<>();
        for(int i = 0; i < mRecyclerView.getChildCount(); ++i){

            ViewHolder viewHolder = (ViewHolder) mRecyclerView.findContainingViewHolder(mRecyclerView.getChildAt(i));
            if(viewHolder instanceof TextViewHolder){

                values.put(mAdapter.getItemByPosition(i).getName(), ((TextViewHolder)viewHolder).getText());
            } else if(viewHolder instanceof NumericViewHolder){

                values.put(mAdapter.getItemByPosition(i).getName(), String.valueOf(((NumericViewHolder) viewHolder).getNumber()));
            } else {

                values.put(mAdapter.getItemByPosition(i).getName(), ((ListViewHolder)viewHolder).getValue());
            }
        }

        return new ClientRequest(values);
    }

    private void fillRecyclerView(Form form){

        mAdapter = new ComplexRecyclerAdapter(getActivity(), form.getFields());
        mRecyclerView.setAdapter(mAdapter);
        mFormTitleTv.setText(form.getTitle());
    }

    @Override
    public void onFormResponse(Form form) {

        if(form != null){

            mForm = form;
            fillRecyclerView(mForm);
        }

        ((MainActivity)getActivity()).getApplicationCore().getDialogManager().hideProgressDialog();
    }

    @Override
    public void onClientResult(ClientResult result) {

        if((result != null) && (!result.isEmpty())){

            ((MainActivity)getActivity()).getApplicationCore().getDialogManager().showResultDialog(result.getResult(), getActivity());
        }

        ((MainActivity)getActivity()).getApplicationCore().getDialogManager().hideProgressDialog();
    }

}
